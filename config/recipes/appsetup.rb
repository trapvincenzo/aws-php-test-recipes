node[:deploy].each do |app_name, deploy|
	script "install_composer" do
		interpreter "bash"
		user "root"
		cwd "#{deploy[:deploy_to]}/current"
		code <<-EOH
		curl -sS https://getcomposer.org/installer | php
		php composer.phar install --no-dev
		EOH
	end
	
	template "#{deploy[:deploy_to]}/current/config/redis.php" do
		source "redis.php.erb"
		mode 0660
		group deploy[:group]

		if platform?("ubuntu")
      		owner "www-data"
    	elsif platform?("amazon")   
      		owner "apache"
    	end

    	variables(
    		:redis => (deploy[:redis])
    	)
    end
end   	